.. -*- coding: utf-8 -*-

Changes
-------

0.10 (2016-03-24)
~~~~~~~~~~~~~~~~~

- Fix insertion of new duty, when the activity has payloads

- Fix the date field range limit on tasks and availabilities


0.9 (2016-03-04)
~~~~~~~~~~~~~~~~

- Easier way to filter persons on their preferred activities


0.8 (2016-03-01)
~~~~~~~~~~~~~~~~

- Add activities and duties payloads management windows


0.7 (2016-02-23)
~~~~~~~~~~~~~~~~

- Re-release due to PyPI failure


0.6 (2016-02-23)
~~~~~~~~~~~~~~~~

- Fix locations default ordering

- Allow filtering persons by preferred activity


0.5 (2016-02-21)
~~~~~~~~~~~~~~~~

- A person is allowed to not have any preferred activity

- Annotations on each entity are now plain text, not HTML


0.4 (2016-02-20)
~~~~~~~~~~~~~~~~

- Fix duties printouts

- Limit the range of time field lookups

- Prevent deletion of records with children

- Implement person's preferred activities management

- Add duties summary printout


0.3 (2016-02-17)
~~~~~~~~~~~~~~~~

- Add YAML and Excel export of a single edition


0.2 (2016-02-16)
~~~~~~~~~~~~~~~~

- Adequate capabilities for the first demo


0.1 (unreleased)
~~~~~~~~~~~~~~~~

- Initial effort
