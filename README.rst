.. -*- coding: utf-8 -*-
.. :Project:   hurm -- Human Resources Manager
.. :Created:   lun 01 feb 2016 20:07:31 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016 Lele Gaifax
..

=========================
 Human Resources Manager
=========================

ExtJS application frontend
==========================

 :author: Lele Gaifax
 :contact: lele@metapensiero.it
 :license: GNU General Public License version 3 or later

This module implements a Pyramid server and an ExtJS based frontend application.
